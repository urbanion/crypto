package com.holoceo.crypto.tokens.data

import com.holoceo.crypto.tokens.data.api.BalanceApi
import com.holoceo.crypto.tokens.data.api.BalanceResponse
import com.holoceo.crypto.tokens.data.api.TokensApi
import com.holoceo.crypto.tokens.data.database.TokenDao
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever
import java.util.concurrent.TimeUnit

class TokenRepositoryImplTest {
    private val tokensApi: TokensApi = mock()
    private val balanceApi: BalanceApi = mock()
    private val tokenDao: TokenDao = mock()
    private val computationScheduler = TestScheduler()
    private lateinit var repository: TokenRepositoryImpl

    @Before
    fun setUp() {
        repository = TokenRepositoryImpl(tokensApi, balanceApi, tokenDao, WALLET_ADDRESS)
        RxJavaPlugins.setComputationSchedulerHandler { computationScheduler }
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
    }

    @Test
    fun `balances are updated by chunks of size 5 every second`() {
        val addresses = (1..13).map { it.toString() }

        val balance = 123L

        whenever(tokenDao.getAllAddresses())
            .thenReturn(Single.just(addresses))

        whenever(balanceApi.getBalance(any(), any()))
            .thenReturn(Single.just(BalanceResponse("OK", balance.toString())))

        whenever(tokenDao.updateBalance(any(), any()))
            .thenReturn(Completable.complete())

        repository.refreshBalances().subscribe()

        val addressesChunks = addresses.chunked(TokenRepositoryImpl.BALANCE_API_CALLS_PER_PERIOD)

        computationScheduler.advanceTimeBy(100, TimeUnit.MILLISECONDS)

        addressesChunks.forEach { chunkAddresses ->
            chunkAddresses.forEach { address ->
                verify(balanceApi).getBalance(address, WALLET_ADDRESS)
            }
            computationScheduler.advanceTimeBy(TokenRepositoryImpl.BALANCE_API_PERIOD_SEC, TimeUnit.SECONDS)
        }
    }

    companion object {
        private const val WALLET_ADDRESS = "wallet"
    }
}