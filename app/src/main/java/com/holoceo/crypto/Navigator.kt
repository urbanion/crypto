package com.holoceo.crypto

import androidx.fragment.app.FragmentManager
import com.holoceo.crypto.intro.IntroFragment
import com.holoceo.crypto.intro.IntroRouter
import com.holoceo.crypto.tokens.presentation.TokensFragment
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class Navigator @Inject constructor() : IntroRouter {

    var fragmentManager: FragmentManager? = null

    fun showIntro() {
        fragmentManager
            ?.beginTransaction()
            ?.add(R.id.container, IntroFragment())
            ?.commit()
    }

    override fun showTokens() {
        fragmentManager
            ?.beginTransaction()
            ?.replace(R.id.container, TokensFragment())
            ?.commit()
    }
}