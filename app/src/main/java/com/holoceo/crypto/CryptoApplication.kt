package com.holoceo.crypto

import android.app.Application
import com.holoceo.crypto.di.ApplicationComponent
import com.holoceo.crypto.di.DaggerApplicationComponent

class CryptoApplication : Application() {

    private lateinit var applicationComponent: ApplicationComponent

    override fun onCreate() {
        super.onCreate()
        applicationComponent = DaggerApplicationComponent.factory().create(applicationContext)
    }

    fun getApplicationComponent(): ApplicationComponent = applicationComponent
}