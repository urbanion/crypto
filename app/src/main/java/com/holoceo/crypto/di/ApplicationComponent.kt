package com.holoceo.crypto.di

import android.content.Context
import com.holoceo.crypto.Navigator
import com.holoceo.crypto.intro.IntroRouter
import com.holoceo.crypto.tokens.domain.TokenInteractor
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [DataModule::class, NavigationModule::class])
interface ApplicationComponent {

    fun getInteractor(): TokenInteractor

    @WalletAddress
    fun getAddress(): String

    fun getIntroRouter(): IntroRouter

    fun getNavigator(): Navigator

    @Component.Factory
    interface Factory {
        fun create(
            @BindsInstance
            @ApplicationContext
            context: Context
        ): ApplicationComponent
    }
}