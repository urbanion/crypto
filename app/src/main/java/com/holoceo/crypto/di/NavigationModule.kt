package com.holoceo.crypto.di

import com.holoceo.crypto.Navigator
import com.holoceo.crypto.intro.IntroRouter
import dagger.Binds
import dagger.Module

@Module
abstract class NavigationModule {

    @Binds
    abstract fun bindRouter(navigator: Navigator): IntroRouter
}