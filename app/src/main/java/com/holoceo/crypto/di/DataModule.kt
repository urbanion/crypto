package com.holoceo.crypto.di

import android.content.Context
import androidx.room.Room
import com.google.gson.Gson
import com.holoceo.crypto.tokens.data.TokenRepositoryImpl
import com.holoceo.crypto.tokens.data.api.BalanceApi
import com.holoceo.crypto.tokens.data.api.TokensApi
import com.holoceo.crypto.tokens.data.database.TokenDao
import com.holoceo.crypto.tokens.data.database.TokenDatabase
import com.holoceo.crypto.tokens.domain.TokenRepository
import dagger.Module
import dagger.Provides
import dagger.Binds
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
abstract class DataModule {

    @Binds
    @Singleton
    abstract fun bindTokensRepository(repository: TokenRepositoryImpl): TokenRepository


    @Module
    companion object {

        @Provides
        @Singleton
        @WalletAddress
        fun provideWalletAddress(): String = "0xde57844f758a0a6a1910a4787ab2f7121c8978c3"

        @Provides
        @Singleton
        fun provideBalanceApi(okHttpClient: OkHttpClient) =
            createApi(
                okHttpClient = okHttpClient,
                baseUrl = "https://api.etherscan.io/",
                service = BalanceApi::class.java
            )

        @Provides
        @Singleton
        fun provideTokensApi(okHttpClient: OkHttpClient) =
            createApi(
                okHttpClient = okHttpClient,
                baseUrl = "https://api.ethplorer.io/",
                service = TokensApi::class.java
            )

        @Provides
        @Singleton
        fun provideHttpClient(): OkHttpClient =
            OkHttpClient.Builder()
                .addInterceptor(
                    HttpLoggingInterceptor().apply {
                        level = HttpLoggingInterceptor.Level.BODY
                    }
                )
                .build()

        @Provides
        @Singleton
        fun provideTokenDao(@ApplicationContext context: Context): TokenDao =
            Room.databaseBuilder(
                context,
                TokenDatabase::class.java,
                "token"
            ).build().tokenDao()

        private fun <T> createApi(
            okHttpClient: OkHttpClient,
            baseUrl: String,
            service: Class<T>
        ): T = Retrofit.Builder()
            .client(okHttpClient)
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(service)

    }
}