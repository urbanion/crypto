package com.holoceo.crypto

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    private val navigator by lazy {
        (application as CryptoApplication).getApplicationComponent().getNavigator()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        navigator.fragmentManager = supportFragmentManager
        if (savedInstanceState == null) navigator.showIntro()
    }

    override fun onDestroy() {
        navigator.fragmentManager = null
        super.onDestroy()
    }
}