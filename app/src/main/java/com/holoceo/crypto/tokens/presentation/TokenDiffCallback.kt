package com.holoceo.crypto.tokens.presentation

import androidx.recyclerview.widget.DiffUtil
import com.holoceo.crypto.tokens.domain.Token

object TokenDiffCallback : DiffUtil.ItemCallback<Token>() {

    override fun areItemsTheSame(oldItem: Token, newItem: Token): Boolean =
        oldItem.address == newItem.address

    override fun areContentsTheSame(oldItem: Token, newItem: Token): Boolean =
        oldItem.balance == newItem.balance
}