package com.holoceo.crypto.tokens.data.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "token")
data class TokenEntity(
    @PrimaryKey val address: String,
    val name: String,
    val symbol: String,
    val decimals: Int,
    val balance: Long?
)