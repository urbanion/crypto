package com.holoceo.crypto.tokens.data

import android.util.Log
import androidx.annotation.VisibleForTesting
import com.holoceo.crypto.di.WalletAddress
import com.holoceo.crypto.tokens.data.api.BalanceApi
import com.holoceo.crypto.tokens.data.api.TokensApi
import com.holoceo.crypto.tokens.data.api.toDomain
import com.holoceo.crypto.tokens.data.database.*
import com.holoceo.crypto.tokens.domain.Token
import com.holoceo.crypto.tokens.domain.TokenRepository
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TokenRepositoryImpl @Inject constructor(
    private val tokensApi: TokensApi,
    private val balanceApi: BalanceApi,
    private val tokenDao: TokenDao,
    @WalletAddress private val walletAddress: String
) : TokenRepository {

    override fun observeTokens(query: String): Observable<List<Token>> =
        tokenDao
            .search(query)
            .map { it.toDomain() }
            .doOnNext { Log.v("custom", "query = $query") }
            .subscribeOn(Schedulers.io())

    override fun refreshTokens(): Completable =
        tokensApi
            .getTokens()
            .map { it.tokens.toDomain().toEntity() }
            .flatMapCompletable { tokenDao.insertAll(it) }
            .subscribeOn(Schedulers.io())

    override fun refreshBalances(): Completable =
        tokenDao
            .getAllAddresses()
            .flatMapCompletable { addresses ->
                Observable
                    .zip(
                        Observable.interval(0, BALANCE_API_PERIOD_SEC, TimeUnit.SECONDS),
                        Observable.fromIterable(addresses.chunked(BALANCE_API_CALLS_PER_PERIOD)),
                        { _, chunkAddresses -> chunkAddresses }
                    )
                    .flatMapCompletable { chunkAddresses ->
                        updateBalances(chunkAddresses)
                    }
            }
            .subscribeOn(Schedulers.io())

    private fun updateBalances(addresses: List<String>): Completable =
        Completable.merge(
            addresses.map { address ->
                updateBalance(address)
            }
        )

    private fun updateBalance(address: String): Completable =
        balanceApi
            .getBalance(address, walletAddress)
            .flatMapCompletable { response ->
                tokenDao.updateBalance(address, response.balance.toLong())
            }
            .subscribeOn(Schedulers.io())
            .onErrorComplete()

    //Balance api has a rate limit of 5 calls per sec/IP
    companion object {

        @VisibleForTesting
        const val BALANCE_API_PERIOD_SEC = 1L

        @VisibleForTesting
        const val BALANCE_API_CALLS_PER_PERIOD = 5
    }
}