package com.holoceo.crypto.tokens.domain

import io.reactivex.Completable
import io.reactivex.Observable

interface TokenRepository {

    fun observeTokens(query: String): Observable<List<Token>>

    fun refreshTokens(): Completable

    fun refreshBalances(): Completable
}