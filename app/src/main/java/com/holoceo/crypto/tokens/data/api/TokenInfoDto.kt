package com.holoceo.crypto.tokens.data.api

import com.google.gson.annotations.SerializedName

class TokenInfoDto(
    @SerializedName("address") val address: String?,
    @SerializedName("name") val name: String?,
    @SerializedName("symbol") val symbol: String?,
    @SerializedName("decimals") val decimals: Int?
)