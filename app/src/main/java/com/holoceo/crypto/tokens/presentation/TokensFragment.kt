package com.holoceo.crypto.tokens.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.hannesdorfmann.adapterdelegates4.AsyncListDifferDelegationAdapter
import com.holoceo.crypto.CryptoApplication
import com.holoceo.crypto.databinding.FragmentTokensBinding
import com.holoceo.crypto.tokens.domain.State

class TokensFragment : Fragment() {

    private var _binding: FragmentTokensBinding? = null

    private val binding get() = _binding!!

    private val viewModel by lazy {
        ViewModelProvider(this, TokensViewModelFactory(requireActivity().application as CryptoApplication))
            .get(TokensViewModel::class.java)
    }

    private val adapter = AsyncListDifferDelegationAdapter(
        TokenDiffCallback,
        tokenAdapterDelegate()
    )

    private var currentView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTokensBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.list.layoutManager = LinearLayoutManager(context)
        binding.list.adapter = adapter
        viewModel.getState().observe(viewLifecycleOwner, ::render)
        binding.btnRetry.setOnClickListener { viewModel.onRetryClicked() }
        binding.edit.addTextChangedListener { viewModel.onQueryChanged(it?.toString() ?: "") }
        binding.refresh.setOnRefreshListener { viewModel.onRefresh() }
    }

    private fun render(state: State) {
        when {
            state.tokens.isNotEmpty() -> {
                changeView(binding.list)
                adapter.items = state.tokens
                binding.refresh.isRefreshing = state.loading
                if (state.error != null) {
                    Toast.makeText(context, getErrorMessageResId(state.error), Toast.LENGTH_SHORT).show()
                }
            }
            state.loading -> changeView(binding.progress)
            state.error != null -> {
                changeView(binding.error)
                binding.errorMessage.setText(getErrorMessageResId(state.error))
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun changeView(view: View) {
        if (currentView !== view) {
            currentView?.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_out))
            view.startAnimation(AnimationUtils.loadAnimation(context, android.R.anim.fade_in))
            currentView?.visibility = View.GONE
            view.visibility = View.VISIBLE
            currentView = view
        }
    }
}