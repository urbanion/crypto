package com.holoceo.crypto.tokens.presentation

import com.holoceo.crypto.R
import java.net.UnknownHostException

fun getErrorMessageResId(error: Throwable): Int =
    if (error is UnknownHostException) {
        R.string.error_no_internet
    } else {
        R.string.error_generic
    }