package com.holoceo.crypto.tokens.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.Single

@Dao
interface TokenDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(tokens: List<TokenEntity>): Completable

    @Query("UPDATE token SET balance = :balance WHERE address = :address")
    fun updateBalance(address: String, balance: Long): Completable

    @Query("SELECT * FROM token WHERE symbol LIKE '%' || :query || '%'")
    fun search(query: String): Observable<List<TokenEntity>>

    @Query("SELECT address FROM token")
    fun getAllAddresses(): Single<List<String>>
}