package com.holoceo.crypto.tokens.data.api

import com.google.gson.annotations.SerializedName

class TokensResponse(
    @SerializedName("tokens") val tokens: List<TokenInfoDto>
)