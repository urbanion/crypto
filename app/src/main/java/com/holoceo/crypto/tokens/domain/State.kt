package com.holoceo.crypto.tokens.domain

data class State(
    val tokens: List<Token> = emptyList(),
    val error: Throwable? = null,
    val loading: Boolean = false
)