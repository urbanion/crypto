package com.holoceo.crypto.tokens.data.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface BalanceApi {

    @GET("api?module=account&action=tokenbalance&tag=latest&apikey=E5QFXD7ZYRH7THQM5PIXB8JD4GY38SEJZ4")
    fun getBalance(
        @Query("contractaddress") contractAddress: String,
        @Query("address") address: String
    ): Single<BalanceResponse>
}