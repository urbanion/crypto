package com.holoceo.crypto.tokens.data.database

import com.holoceo.crypto.tokens.domain.Token

fun TokenEntity.toDomain(): Token = Token(address, name, symbol, decimals, balance)

fun List<TokenEntity>.toDomain(): List<Token> = map { it.toDomain() }

fun Token.toEntity(): TokenEntity = TokenEntity(address, name, symbol, decimals, balance)

fun List<Token>.toEntity(): List<TokenEntity> = map { it.toEntity() }