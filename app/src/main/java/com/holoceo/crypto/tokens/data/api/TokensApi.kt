package com.holoceo.crypto.tokens.data.api

import io.reactivex.Single
import retrofit2.http.GET

interface TokensApi {

    @GET("getTopTokens?limit=100&apiKey=freekey")
    fun getTokens(): Single<TokensResponse>
}