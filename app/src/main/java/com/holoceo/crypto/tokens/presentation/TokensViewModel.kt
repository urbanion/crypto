package com.holoceo.crypto.tokens.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.holoceo.crypto.tokens.domain.State
import com.holoceo.crypto.tokens.domain.TokenInteractor
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject

class TokensViewModel(tokenInteractor: TokenInteractor) : ViewModel() {

    private val state = MutableLiveData<State>()
    private val searchQuery = PublishSubject.create<String>()
    private val refreshSubject = PublishSubject.create<Unit>()
    private val disposable: Disposable

    init {
        disposable = tokenInteractor
            .observeState(searchQuery, refreshSubject)
            .subscribe {
                state.postValue(it)
            }
    }

    fun getState(): LiveData<State> = state

    fun onRetryClicked() {
        refreshSubject.onNext(Unit)
    }

    fun onRefresh() {
        refreshSubject.onNext(Unit)
    }

    fun onQueryChanged(query: String) {
        searchQuery.onNext(query)
    }

    override fun onCleared() {
        disposable.dispose()
    }
}