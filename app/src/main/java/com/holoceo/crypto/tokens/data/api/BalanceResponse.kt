package com.holoceo.crypto.tokens.data.api

import com.google.gson.annotations.SerializedName

class BalanceResponse(
    @SerializedName("status") val status: String,
    @SerializedName("result") val balance: String
)