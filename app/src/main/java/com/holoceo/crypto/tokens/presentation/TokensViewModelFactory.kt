package com.holoceo.crypto.tokens.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.holoceo.crypto.CryptoApplication

class TokensViewModelFactory(private val application: CryptoApplication) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val appComponent = application.getApplicationComponent()

        if (modelClass.isAssignableFrom(TokensViewModel::class.java)) {
            return TokensViewModel(
                tokenInteractor = appComponent.getInteractor()
            ) as T
        } else {
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}