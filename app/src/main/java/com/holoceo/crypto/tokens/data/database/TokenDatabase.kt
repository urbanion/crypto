package com.holoceo.crypto.tokens.data.database

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = arrayOf(TokenEntity::class), version = 1)
abstract class TokenDatabase : RoomDatabase() {
    abstract fun tokenDao(): TokenDao
}