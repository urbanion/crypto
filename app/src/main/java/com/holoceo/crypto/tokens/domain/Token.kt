package com.holoceo.crypto.tokens.domain

data class Token(
    val address: String,
    val name: String,
    val symbol: String,
    val decimals: Int,
    val balance: Long?
)