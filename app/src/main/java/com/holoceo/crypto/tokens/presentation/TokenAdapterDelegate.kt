package com.holoceo.crypto.tokens.presentation

import androidx.core.content.ContextCompat
import com.hannesdorfmann.adapterdelegates4.dsl.adapterDelegateViewBinding
import com.holoceo.crypto.R
import com.holoceo.crypto.databinding.ItemTokenBinding
import com.holoceo.crypto.tokens.domain.Token
import java.math.BigDecimal
import java.text.DecimalFormat

fun tokenAdapterDelegate() = adapterDelegateViewBinding<Token, Token, ItemTokenBinding>(
    { layoutInflater, root -> ItemTokenBinding.inflate(layoutInflater, root, false) }
) {
    bind {
        binding.title.text = context.resources.getString(R.string.item_title, item.symbol)

        val balance = item.balance
        if (balance != null) {
            val color = if (balance > 0) R.color.balance_positive else R.color.balance_zero
            binding.value.setTextColor(ContextCompat.getColor(context, color))
            binding.value.text = if (balance > 0) BigDecimal.valueOf(balance, item.decimals).toString() else "0"
        } else {
            binding.value.text = ""
        }
    }
}