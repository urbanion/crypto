package com.holoceo.crypto.tokens.domain

import io.reactivex.Observable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class TokenInteractor @Inject constructor(
    private val tokenRepository: TokenRepository
) {

    fun observeState(
        searchQuery: Observable<String>,
        refreshEvents: Observable<Unit>
    ): Observable<State> =
        Observable
            .combineLatest(
                observeTokens(searchQuery),
                observeRefrashStatus(refreshEvents)
            ) { tokens, refreshStatus ->
                State(
                    tokens = tokens,
                    loading = refreshStatus == RefreshStatus.Loading,
                    error = (refreshStatus as? RefreshStatus.Error)?.throwable
                )
            }
            .distinctUntilChanged()

    private fun observeTokens(searchQuery: Observable<String>): Observable<List<Token>> =
        searchQuery
            .debounce(SEARCH_DEBOUNCE_MILLIS, TimeUnit.MILLISECONDS)
            .startWith("")
            .switchMap { query -> tokenRepository.observeTokens(query) }

    fun observeRefrashStatus(refreshEvents: Observable<Unit>): Observable<RefreshStatus> =
        refreshEvents
            .startWith(Unit)
            .switchMap {
                tokenRepository
                    .refreshTokens()
                    .andThen(tokenRepository.refreshBalances())
                    .andThen(Observable.just<RefreshStatus>(RefreshStatus.Loaded))
                    .onErrorReturn { throwable -> RefreshStatus.Error(throwable) }
                    .startWith(RefreshStatus.Loading)
            }


    sealed class RefreshStatus {
        object Loading : RefreshStatus()
        object Loaded : RefreshStatus()
        class Error(val throwable: Throwable) : RefreshStatus()
    }

    companion object {
        const val SEARCH_DEBOUNCE_MILLIS = 200L
    }
}