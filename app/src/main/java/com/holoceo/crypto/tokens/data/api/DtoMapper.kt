package com.holoceo.crypto.tokens.data.api

import com.holoceo.crypto.tokens.domain.Token
import java.lang.IllegalArgumentException

fun TokenInfoDto.toDomain(): Token =
    Token(
        requireNotNull(address),
        requireNotNull(name),
        requireNotNull(symbol),
        requireNotNull(decimals),
        null
    )

fun List<TokenInfoDto>.toDomain(): List<Token> =
    mapNotNull { tokenDto ->
        try {
            tokenDto.toDomain()
        } catch (e: IllegalArgumentException) {
            null
        }
    }