package com.holoceo.crypto.intro

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.holoceo.crypto.CryptoApplication

class IntroViewModelFactory(private val application: CryptoApplication) : ViewModelProvider.Factory {

    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        val appComponent = application.getApplicationComponent()

        if (modelClass.isAssignableFrom(IntroViewModel::class.java)) {
            return IntroViewModel(
                walletAddress = appComponent.getAddress(),
                router = appComponent.getIntroRouter()
            ) as T
        } else {
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }
}