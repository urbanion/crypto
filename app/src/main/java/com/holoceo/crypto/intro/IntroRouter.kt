package com.holoceo.crypto.intro

interface IntroRouter {
    fun showTokens()
}