package com.holoceo.crypto.intro

import androidx.lifecycle.ViewModel

class IntroViewModel(
    private val walletAddress: String,
    private val router: IntroRouter
) : ViewModel() {

    fun getWalletAddress(): String = walletAddress

    fun showTokens() {
        router.showTokens()
    }
}