How long did you spend on the exercise?
- 10 hours

What would you improve if you had more time?
- Investigate why etherscan rate limit of 5 calls per sec/IP does not work
- Display error/loading state for specific token (just need to add 2 fields to the model)
- Clean up DI (I left it simple because there is only one feature/screen), maybe tried Hilt
- Use different approach for showing error
- Move ui mapping (error, list item) to presentation model
- Write more Unit tests
- Write UI tests

What would you like to highlight in the code?
- Etherscan claims to have limit of 5 calls per sec/IP. However it seems not to be true.
- I wrote test to cover this - TokenRepositoryImplTest

If you had to store the private key associated with an ethereum account on an
android device, how would you make that storage secure?
- I would download it from the backend and store it in the KeyStore
- If account should be bundled with the app, then I would hide it in the native code